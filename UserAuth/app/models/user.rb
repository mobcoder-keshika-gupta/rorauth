class User < ApplicationRecord

    validates :username, presence: true,
                                length: { minimum: 2, maximum: 40 }
    validates :username, uniqueness: true
    has_secure_password
    has_many :books
end
