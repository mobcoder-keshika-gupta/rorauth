class UsersController < ApplicationController
    skip_before_action :verify_authenticity_token
    http_basic_authenticate_with :name => "user", :password => "user"


    def create
        @user= User.create(user_params)

        if @user.save
            render json: { status:"Success", message:"created user", data:@user}, status: :ok

        else
            render json: { status:"Error", message:"Error occured", data:@user.errors}, status: :unprocessable_entity
        end
    end


    def login
        @user = User.find_by(username:user_params[:username])

        if @user && @user.authenticate(user_params[:password])

            token = encode_token({user_id: @user.id , email: @user.email})

            time = Time.now + 5.minute.to_i

            UserHelper.set(token,@user.id)

            render json: { status:"Success", message:"logged in", token: token}, status: :ok

        else
            render json: { status:"Error", message:"Invalid username or password"}, status: :unprocessable_entity

        end
    end



    def logout

        tok = request.headers['AccessToken']
        if tok
            token = tok.split(' ')[1]

            u = UserHelper.delete(token)

            puts "/////////////////"
            puts u
            if u
              
                render json: { message: 'sign out successfully' , data: u}, status: 200
            else
                 render json: { message: 'Error in sign out '}, status: 400
              
            end
        end


      
    end



    private

    def user_params
        params.require(:user).permit(:username, :password, :email)
    end
end
