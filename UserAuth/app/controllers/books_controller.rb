class BooksController < ApplicationController
    before_action :authorize
    before_action :find_book, only: [:show, :update, :destroy]
    skip_before_action :verify_authenticity_token
    http_basic_authenticate_with :name => "user", :password => "user"


    puts @user
    

    def index
        @books= @user.books.all
        render json: { status:"Success", message:"Loaded books", user: @user.username ,  data:@books}, status: :ok
    end


    def show
        render json: { status:"Success", message:"Loaded articles", data:@book}, status: :ok
    end


    def create 
        @book = Book.new(book_params.merge(user: @user))
        if @book.save
            render json: { status:"Success", message:"added book", data:@book}, status: :ok
        else
            render json: { status:"Error", message:"Error occured", data:@book.errors}, status: :unprocessable_entity
        end

    end


    def update
    
        if @book.update(book_params)
            render json: { status:"Success", message:"updated article", data:@book}, status: :ok

        else
            render json: { status:"Error", message:"Error occured in updating", data:@book.errors}, status: :unprocessable_entity

        end
    end


    def destroy
        if @book.destroy
            render json: { status:"Success", message:"deleted article"}, status: :ok

        else
            render json: { status:"Error", message:"Error occured in deleting", data:book.errors}, status: :unprocessable_entity

        end

    end


    private

    def find_book
        @book = @user.books.find(params[:id])
      end

    def book_params
        params.require(:book).permit(:title)
    end

end
