class ApplicationController < ActionController::Base


    def encode_token(payload , exp = 5.minute.from_now)
        payload[:exp] = exp.to_i
        JWT.encode(payload, "secret")
    end



    def decode_token
        auth = authenticate_with_token()
        begin
            JWT.decode(auth, 'secret', true , algorithm: 'HS256')
        rescue JWT::DecodeError
            nil
        end
     
    end



    def authorized_user
        decoded_token = decode_token()
        if decoded_token
            user_id = decoded_token[0]['user_id']
            email = decoded_token[1]['email']
            @user = User.find_by(id: user_id)
        end
    end


    def authorize
        render json: {message:'Unauthorized'}, status: :unauthorized unless 
        authorized_user
    end



    def authenticate_with_token

        tok = request.headers['AccessToken']
        if tok
            token = tok.split(' ')[1]

            if UserHelper.get(token)
               
              return token
            else
                return
                render json: {message:'Unauthorized token'}, status: 400
            end
        end
      
      end

end

