module UserHelper

        NAMESPACE = 'redis_use:'
      
        class << self
          def set(key, value)
            $redis.set("#{NAMESPACE}#{key}", value)
            expire(key)
          end
      
    
          def get(key)
            $redis.get("#{NAMESPACE}#{key}")
          end
      
    
          def delete(key)
            return false unless exist?(key)
            $redis.del("#{NAMESPACE}#{key}")
            true
          end
      
    
          def exist?(key)
            $redis.exists("redis_use:#{key}")
          end
      
          
          private
            def expire(key)
              $redis.expire("#{NAMESPACE}#{key}", 5.minute)
            end
        end
   
end
